import random

import pandas as pd

import redmine

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
import numpy as np
from random import shuffle, randint, choice
import collections
import string
#import openpyxl
import xlrd
import time
import datetime
from datetime import timedelta
from pandas import ExcelWriter
import xlwings as xw
from shutil import copy2
from collections import defaultdict

import os

datum = "4. MRNTZ 2022/23, Logatec, 26.2.2022"
IN_FOLDER = "IN/Templates/2023"
dirlist = os.listdir(IN_FOLDER)
print(dirlist)

data = []
for file in dirlist:
    if file.startswith("MRNTZ"):
        data.append(file)

#data = ['MRNTZ.2017.b4.xlsx', 'MRNTZ.2017.g4.xlsx']

title_categories = {
"B,ABxs":"B,ABxs (učenke)",
"ABxs":'ABxs učenke in učenci 1. razred',
"A":'A učenci 2.-3. razred',
"C":'C učenci 4.-5. razred',
"E":'E učenci 6.-7. razred',
"G":'G učenci 8.-9. razred',
"I":'I dijaki',
"B":'B učenke 2.-3. razred',
"D":'D učenke 4.-5. razred',
"F":'F učenke 6.-7.razred',
"H":'H učenke 8.-9. razred',
"J":'J dijakinje'
}

title_categories_girls = {
#"B":'B učenke 2.-3. razred',
"B,ABxs":"B,ABxs (učenke 1-3. razred)",
"D":'D učenke 4.-5. razred',
"F":'F učenke 6.-7.razred',
"H":'H učenke 8.-9. razred',
"J":'J dijakinje'
}

input_sheet_names = [
    #'ABxs (učenke in učenci 1. razr)',
    #'A (učenci 2.-3.)',
    #'C (učenci 4.-5.)',
    #'E (učenci 6.-7.)',
    #'G (učenci 8.-9.)',
    #'I (dijaki)',
    #'B (učenke 2.-3.)',
    #'B,ABxs (učenke)',
    #'D (učenke 4.-5.)',
    #'F (učenke 6.-7.)',
    #'H (učenke 8.-9.)',
    #'J (dijakinje)'
]

input_sheet_names_boys = [
    #'ABxs (učenke in učenci 1. razr)',
    'A,ABxs učenci 1.-3.',
    'C (učenci 4.-5.)',
    'E (učenci 6.-7.)',
    'G (učenci 8.-9.)',
    'I (dijaki)',
    #'B (učenke 2.-3.)',
    #'B,ABxs (učenke)',
    #'D (učenke 4.-5.)',
    #'F (učenke 6.-7.)',
    #'H (učenke 8.-9.)',
    #'J (dijakinje)'
]

input_sheet_names_girls = [
    #'ABxs (učenke in učenci 1. razr)',
    #'A,ABxs učenci 1.-3.',
    #'C (učenci 4.-5.)',
    #'E (učenci 6.-7.)',
    #'G (učenci 8.-9.)',
    #'I (dijaki)',
    #'B (učenke 2.-3.)',
    'B,ABxs (učenke)',
    'D (učenke 4.-5.)',
    'F (učenke 6.-7.)',
    'H (učenke 8.-9.)',
    'J (dijakinje)'
]

contestants_number_groups_models = {
    1:[1], #OK
    2:[2], #OK
    3:[3],#OK
    4:[4],#OK
    5:[5],#OK
    6:[3,3],#OK
    7:[4,3],#OK
    8:[4,4],#OK
    9:[5,4],#OK
    10:[5,5],#OK
    11:[4,4,3],#OK
    12:[4,4,4],#OK
    13:[5,4,4],#OK
    14:[4,4,3,3], #OK
    15:[4,4,4,3], #OK
    16:[4,4,4,4],#OK
    17:[4,4,3,3,3], # same as 18 #OK [5,4,4,4],
    18:[4,4,4,3,3],#OK
    19:[4,4,4,4,3],#OK
    20:[4,4,4,4,4],#OK
    21:[4,4,4,3,3,3],#OK
    22:[4,4,4,4,3,3],#OK
    23:[4,4,4,4,4,3],#OK
    24:[4,4,4,4,4,4],#OK,
    30:[4,4,4,4,4,4,3,3],#OK,
    31:[4,4,4,4,4,4,4,3],#OK,
    32:[4,4,4,4,4,4,4,4],#OK,đ
    35:[4,4,4,4,4,3,4,4,4],#OK,đ
    45:[4,4,4,4,4,4,3,4,4,4,3,3],
    46:[4,4,4,4,4,4,4,4,4,4,3,3],
    47:[4,4,4,4,4,4,4,4,4,4,4,3],
    48:[4,4,4,4,4,4,4,4,4,4,4,4],
    51:[4,4,4,4,4,4,4,4,4,4,4,4,3],

}

def check_clubs_can_insert(player, club, group):
    number_of_players_from_same_club = 0
    for player_from_group in group:
        if player_from_group:
            player_from_group_club = player_from_group.split("-")[1]
            if player_from_group_club == club:
                return False
        else:
            return True

def correct_clubs(groups):
    dict_count = defaultdict(dict)


    for f, gr in enumerate(groups):
        dict_clubs = collections.defaultdict(int)
        for pl in gr:
            dict_clubs[pl.split("-")[1]] += 1
        dict_count[str(f)]=dict_clubs
    print(dict_count)

def insert_player(player, groups, club):
    slot_in_group = 9999

    first_poition_free = False
    third_poition_free = False
    second_poition_free = False

    #check first positions
    for  i ,group in enumerate(groups):
        if group[0]== 0:
            first_poition_free = True
            group_index = i
            group[0] = player+"-"+club
            break

    if first_poition_free == False:

        third_poition_free = False
        third_poition_free_indices = []
        #check third position
        for j,group in enumerate(groups):
            if group[2]== 0:
                third_poition_free = True
                third_poition_free_indices.append(j)
        if third_poition_free:
            groups[random.choice(third_poition_free_indices)][2] = player+"-"+club
            return

    if first_poition_free == False and third_poition_free == False:
        second_poition_free = False
        second_poition_free_indices = []
        # check second position
        for p, group in enumerate(groups):
            if group[1] == 0:
                second_poition_free = True
                second_poition_free_indices.append(p)
        if second_poition_free:
            groups[random.choice(second_poition_free_indices)][1] = player + "-" + club
            return


    if first_poition_free == False and third_poition_free == False and second_poition_free == False:
        fourth_poition_free = False
        fourth_poition_free_indices = []
        # check fourth position
        for k,group in enumerate(groups):
            if len(group)> 3:
                if group[3] == 0:
                    fourth_poition_free = True
                    fourth_poition_free_indices.append(k)
        if fourth_poition_free:
            groups[random.choice(fourth_poition_free_indices)][3] = player + "-" + club
            return

    if first_poition_free == False and third_poition_free == False and second_poition_free == False and fourth_poition_free == False:
        fifth_poition_free = False
        fifth_poition_free_indices = []
        # check fifth position
        for m,group in enumerate(groups):
            if len(group) > 4:
                if group[4] == 0:
                    fifth_poition_free = True
                    fifth_poition_free_indices.append(m)
        if first_poition_free:
            groups[random.choice(fifth_poition_free_indices)][4] = player + "-" + club
            return

def write_to_excel(number_of_contestants, groups, category):
    copy2("IN/Templates/"+str(number_of_contestants)+".xls", "OUT/"+category+".xls")
    copy2("IN/Templates/" + str(number_of_contestants) + ".xls", "OUT/" + category + "finalna.xls")
    book = xw.Book("OUT/"+category+".xls")
    book_finalna = xw.Book("OUT/" + category + "finalna.xls")

    #will break excel references
    #book.sheets[0].name = category
    #sheet = book.sheets(category)

    sheet = book.sheets(book.sheet_names[0])
    sheet_finalna = book_finalna.sheets(book_finalna.sheet_names[0])


    print(category)
    sheet.range("A1").value = category + ", "+datum
    sheet_finalna.range("A1").value = category + ", " + datum
    book_finalna.save()
    book_finalna.close()
    counter = 0
    letters = "ABCDEFGHIJKLMNOPRS"
    for row in range(1, 150):
        col = 2
        for let in letters:
            if sheet.range((row, col)).value == "skupina: "+let:

                print("The Row is: " + str(row) + " and the column is " + str(col))
                count_players =2

                for player in groups[counter]:
                    #player name
                    sheet.range(row+count_players,col).value = player.split("-")[0]
                    #club
                    sheet.range(row + count_players, col+10).value = player.split("-")[1]
                    count_players+=2
                counter+=1
    #sheet.range("H11").value = header[0]
    book.save()
    book.close()

"""def write_to_excel(number_of_contestants, df_groups, category):

    #read template
    excel = pd.ExcelFile("IN/Templates/" + str(number_of_contestants) + ".xls")
    sheets = excel.sheet_names

    # Write Title
    title_letter = category.split(" ")[0]
    title_category_class = title_categories[title_letter]
    title_date = datetime.date.today() + timedelta(days=1)

    # rename sheet
    if len(excel.sheet_names) == 3:
        excel.sheet_names[0]=title_letter
        excel.sheet_names[1]=title_letter+"F"
    else:
        excel.sheet_names[0]=title_letter

    title = "("+title_letter+")"+"MRNTZ"+" "+"-"+" "+title_category_class+", "+"LOGATEC"+", "+str(title_date)+""
    data_title = {'title': [title]}
    df_title = pd.DataFrame(data = data_title)
    df_title.to_excel(excel,excel.sheet_names[0], header=False)
    excel.close()

    #print(sheets)
    #print(excel)

    #writer = pd.ExcelWriter("IN/Templates/" + str(number_of_contestants) + ".xls")
    start_row = 5 #1 indexed
    start_column = 2 #1 indexed
    #for group_index in df_groups.index:
        #df_group = df_groups.iloc[group_index]
        #df_group.to_excel(writer,)

        #print(df_group)
        #print(group_index)"""


def generate_draw(data, category):
    e_df =  pd.read_excel( IN_FOLDER+ "\\" +data, category, header = 6,index_col=None)
    old_columns = list(e_df.columns)
    old_columns[4] = 'Klub'
    e_df.columns = old_columns
    e_df =  e_df.drop([0], axis=0)
    e_df = e_df[np.isfinite(e_df['Mesto'])]
    e_df = e_df.dropna(subset=['Priimek in ime'])
    e_df = e_df.dropna(subset=['Logatec'])
    e_df.index = range(len(e_df))
    #e_df = e_df[e_df.Mesto != pd.isnull]

    #randomize the ones with same 'Mesto'
    ranking = None
    dict_for_randomization = {}
    start_index = None

    #randomize those with same ranking
    for player_index in e_df.index:
        if player_index < len(e_df.index)-1:
            ranking = e_df.loc[player_index, 'Mesto']
            ranking_next = e_df.loc[player_index+1, 'Mesto']
            if ranking == ranking_next:
                if not dict_for_randomization:
                    start_index = player_index
                dict_for_randomization[player_index] = e_df.iloc[player_index]
                dict_for_randomization[player_index + 1] = e_df.iloc[player_index + 1]
            else:
                if dict_for_randomization:
                    keys = list(dict_for_randomization.keys())
                    shuffle(keys)
                    for k in keys:
                        e_df.iloc[start_index] = dict_for_randomization[k]
                        start_index+=1
                    dict_for_randomization = {}
        elif player_index == len(e_df.index)-1:
            if dict_for_randomization:
                keys = list(dict_for_randomization.keys())
                shuffle(keys)
                for k in keys:
                    e_df.iloc[start_index] = dict_for_randomization[k]
                    start_index += 1
                dict_for_randomization = {}

    number_of_contestants = len(e_df[e_df['Logatec'] == '+'])  #e_df.loc[e_df['Logatec'] == '+'].sum()

    if number_of_contestants == 0:
        print("Nihče ni prijavljen v kategoriji "+category)
        print()
        return
    else:
        print("Število prijavljenih:" + str(number_of_contestants))

    print(e_df)

    model = contestants_number_groups_models[number_of_contestants]
    groups = []
    for number_in_group in model:
        groups.append(number_in_group * [0])

    for player_index in e_df.index:
        player = e_df.loc[player_index, 'Priimek in ime']
        players_club = e_df.loc[player_index, 'Klub']
        insert_player(player, groups, players_club)
        #print("Vnašam igralca "+player, groups, players_club)
        #print("Stanje skupine po vnosu:")
    print("######")
    for g_r in groups:
        print(g_r)

    #for g in groups:
    #print("######")
    #print(groups)

    df = pd.DataFrame(groups)
    #df.style.set_properties(**{'text-align': 'left'})

    #write_to_excel(number_of_contestants, df, category)

    #print groups
    alphabet = string.ascii_uppercase
    alphabet_list = []
    for char in alphabet:
        alphabet_list.append(char)
    df.index = alphabet_list[:df.shape[0]]

    max_len = 0
    for group in groups:
        max_len = max(len(group),max_len)

    df.columns = list(range(1, max_len+1))

    #write_to_excel(number_of_contestants,groups,category)

    redmine.generate_groups(groups,category)

    """with pd.option_context('display.colheader_justify', 'left'):
       print(df)
    
    print()"""

    correct_clubs(groups)

#for e in input_sheet_names_boys:
#    generate_draw(data[0], e)

#for e in input_sheet_names_girls:
#    generate_draw(data[1], e)

generate_draw(data[0],"I (dijaki)" )