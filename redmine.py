from redminelib import Redmine
import datetime

redmine = Redmine('http://65.108.141.144:3000/', key="a27fa47f5a9f19b097ad19d59849b58ee9598aff")
project_id = "table-tennis-admin"
project = redmine.project.get('table-tennis-admin')

print(project.id)

ids_categories = {
'B,ABxs (učenke)':2,
    'D (učenke 4.-5.)':4,
    'F (učenke 6.-7.)':6,
    'H (učenke 8.-9.)':8,
    'J (dijakinje)':10,
    'A,ABxs učenci 1.-3.':1,
    'C (učenci 4.-5.)':3,
    'E (učenci 6.-7.)':5,
    'G (učenci 8.-9.)':7,
    'I (dijaki)':9,

}

def generate_groups(groups, category):
    chars = "ABCDEFGHIJKLMNOPRSTUVZX"


    for i, group in enumerate(groups):
        #insert first 3 players, check for others
        custom_fields_players_in_group = [{'id': 20, 'value': group[0]}, {'id': 21, 'value': group[1]}, {'id': 22, 'value': group[2]}]
        custom_fields_checklist=[{'subject': '1', 'is_done': True}, {'subject': '2', 'is_done': True},  {'subject': '3', 'is_done': True}]
        if len(group)==4:
            custom_fields_players_in_group.append({'id': 23, 'value': group[3]})
            custom_fields_checklist.append({'subject': '4', 'is_done': True})
        if len(group)==5:
            custom_fields_players_in_group.append({'id': 23, 'value': group[3]})
            custom_fields_players_in_group.append({'id': 24, 'value': group[4]})
            custom_fields_checklist.append({'subject': '4', 'is_done': True})
            custom_fields_checklist.append({'subject': '5', 'is_done': True})

        issue = redmine.issue.create(
            project_id=project_id,
            subject='Skupina '+chars[i],
            tracker_id =4,
            description='Skupina '+chars[i],
            category_id = ids_categories[category],
            status_id=1,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_players_in_group,
            checklists=custom_fields_checklist

        )

        generate_matches(group, chars[i], category, issue.id)


def generate_matches(group, group_name, category, parent_issue_id):


    if len(group) == 4:
        #1. krog 1-4,2-3
        if group[3] != 0:
            custom_fields_match = [{'id': 1, 'value': group[0].split("-")[0]}, {'id': 14, 'value': group[0].split("-")[1]},
                                   {'id': 2, 'value': group[3].split("-")[0]},
                                   {'id': 15, 'value': group[3].split("-")[1]},
                                   {'id': 26, 'value': 1}]
            issue = redmine.issue.create(
                project_id=project_id,
                subject="Skupina:"+group_name+","+"1.krog "+ group[0]+":"+group[3],
                tracker_id=3,
                description="Skupina:"+group_name+","+"1.krog "+ group[0]+":"+group[3],
                category_id=ids_categories[category],
                status_id=1,
                parent_issue_id = parent_issue_id,
                priority_id=1,
                assigned_to_id=1,
                watcher_user_ids=[1],
                custom_fields=custom_fields_match


            )

        custom_fields_match = [{'id': 1, 'value': group[1].split("-")[0]},
                              {'id': 14, 'value': group[1].split("-")[1]},
                              {'id': 2, 'value': group[2].split("-")[0]},
                              {'id': 15, 'value': group[2].split("-")[1]},
                              {'id': 26, 'value': 1}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "1.krog " + group[1] + ":" + group[2],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "1.krog " + group[1] + ":" + group[2],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #2. krog # 4-3,1-2
        if group[3] != 0:
            custom_fields_match = [{'id': 1, 'value': group[3].split("-")[0]},
                                   {'id': 14, 'value': group[3].split("-")[1]},
                                   {'id': 2, 'value': group[2].split("-")[0]},
                                   {'id': 15, 'value': group[2].split("-")[1]},
                                   {'id': 26, 'value': 2}]
            issue = redmine.issue.create(
                project_id=project_id,
                subject="Skupina:" + group_name + "," + "2.krog " + group[3] + ":" + group[2],
                tracker_id=3,
                description="Skupina:" + group_name + "," + "2.krog " + group[3] + ":" + group[2],
                category_id=ids_categories[category],
                status_id=1,
                parent_issue_id=parent_issue_id,
                priority_id=1,
                assigned_to_id=1,
                watcher_user_ids=[1],
                custom_fields=custom_fields_match

            )

        custom_fields_match = [{'id': 1, 'value': group[0].split("-")[0]},
                               {'id': 14, 'value': group[0].split("-")[1]},
                               {'id': 2, 'value': group[1].split("-")[0]},
                               {'id': 15, 'value': group[1].split("-")[1]},
                               {'id': 26, 'value': 2}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "2.krog " + group[0] + ":" + group[1],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "2.krog " + group[0] + ":" + group[1],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #3. # 2-4,3-1
        if group[3] != 0:
            custom_fields_match = [{'id': 1, 'value': group[1].split("-")[0]},
                                   {'id': 14, 'value': group[1].split("-")[1]},
                                   {'id': 2, 'value': group[3].split("-")[0]},
                                   {'id': 15, 'value': group[3].split("-")[1]},
                                   {'id': 26, 'value': 3}]
            issue = redmine.issue.create(
                project_id=project_id,
                subject="Skupina:" + group_name + "," + "3.krog " + group[1] + ":" + group[3],
                tracker_id=3,
                description="Skupina:" + group_name + "," + "3.krog " + group[1] + ":" + group[3],
                category_id=ids_categories[category],
                status_id=1,
                parent_issue_id=parent_issue_id,
                priority_id=1,
                assigned_to_id=1,
                watcher_user_ids=[1],
                custom_fields=custom_fields_match

            )

        custom_fields_match = [{'id': 1, 'value': group[2].split("-")[0]},
                               {'id': 14, 'value': group[2].split("-")[1]},
                               {'id': 2, 'value': group[0].split("-")[0]},
                               {'id': 15, 'value': group[0].split("-")[1]},
                               {'id': 26, 'value': 3}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "3.krog " + group[2] + ":" + group[0],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "3.krog " + group[2] + ":" + group[0],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

    if len(group) == 5:
        #2-5 3-4

        custom_fields_match = [{'id': 1, 'value': group[1].split("-")[0]},
                               {'id': 14, 'value': group[1].split("-")[1]},
                               {'id': 2, 'value': group[4].split("-")[0]},
                               {'id': 15, 'value': group[4].split("-")[1]},
                               {'id': 26, 'value': 1}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "1.krog " + group[1] + ":" + group[4],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "1.krog " + group[1] + ":" + group[4],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        custom_fields_match = [{'id': 1, 'value': group[2].split("-")[0]},
                               {'id': 14, 'value': group[2].split("-")[1]},
                               {'id': 2, 'value': group[3].split("-")[0]},
                               {'id': 15, 'value': group[3].split("-")[1]},
                               {'id': 26, 'value': 1}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "1.krog " + group[2] + ":" + group[3],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "1.krog " + group[2] + ":" + group[3],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #5-3,1-2

        custom_fields_match = [{'id': 1, 'value': group[4].split("-")[0]},
                               {'id': 14, 'value': group[4].split("-")[1]},
                               {'id': 2, 'value': group[2].split("-")[0]},
                               {'id': 15, 'value': group[2].split("-")[1]},
                               {'id': 26, 'value': 2}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "2.krog " + group[4] + ":" + group[2],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "2.krog " + group[4] + ":" + group[2],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        custom_fields_match = [{'id': 1, 'value': group[0].split("-")[0]},
                               {'id': 14, 'value': group[0].split("-")[1]},
                               {'id': 2, 'value': group[1].split("-")[0]},
                               {'id': 15, 'value': group[1].split("-")[1]},
                               {'id': 26, 'value': 2}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "2.krog " + group[0] + ":" + group[1],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "2.krog " + group[0] + ":" + group[1],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #3-1,4-5

        custom_fields_match = [{'id': 1, 'value': group[2].split("-")[0]},
                               {'id': 14, 'value': group[2].split("-")[1]},
                               {'id': 2, 'value': group[0].split("-")[0]},
                               {'id': 15, 'value': group[0].split("-")[1]},
                               {'id': 26, 'value': 3}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "3.krog " + group[2] + ":" + group[0],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "3.krog " + group[2] + ":" + group[0],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        custom_fields_match = [{'id': 1, 'value': group[3].split("-")[0]},
                               {'id': 14, 'value': group[3].split("-")[1]},
                               {'id': 2, 'value': group[4].split("-")[0]},
                               {'id': 15, 'value': group[4].split("-")[1]},
                               {'id': 26, 'value': 3}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "3.krog " + group[3] + ":" + group[4],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "3.krog " + group[3] + ":" + group[4],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #4. 1-4,2-3
        #

        custom_fields_match = [{'id': 1, 'value': group[0].split("-")[0]},
                               {'id': 14, 'value': group[0].split("-")[1]},
                               {'id': 2, 'value': group[3].split("-")[0]},
                               {'id': 15, 'value': group[3].split("-")[1]},
                               {'id': 26, 'value': 4}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "4.krog " + group[0] + ":" + group[3],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "4.krog " + group[0] + ":" + group[3],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        custom_fields_match = [{'id': 1, 'value': group[1].split("-")[0]},
                               {'id': 14, 'value': group[1].split("-")[1]},
                               {'id': 2, 'value': group[2].split("-")[0]},
                               {'id': 15, 'value': group[2].split("-")[1]},
                               {'id': 26, 'value': 4}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "4.krog " + group[1] + ":" + group[2],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "4.krog " + group[1] + ":" + group[2],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        #4-2,5-1
        custom_fields_match = [{'id': 1, 'value': group[3].split("-")[0]},
                               {'id': 14, 'value': group[3].split("-")[1]},
                               {'id': 2, 'value': group[1].split("-")[0]},
                               {'id': 15, 'value': group[1].split("-")[1]},
                               {'id': 26, 'value': 5}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "5.krog " + group[3] + ":" + group[1],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "5.krog " + group[3] + ":" + group[1],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

        custom_fields_match = [{'id': 1, 'value': group[4].split("-")[0]},
                               {'id': 14, 'value': group[4].split("-")[1]},
                               {'id': 2, 'value': group[0].split("-")[0]},
                               {'id': 15, 'value': group[0].split("-")[1]},
                               {'id': 26, 'value': 5}]
        issue = redmine.issue.create(
            project_id=project_id,
            subject="Skupina:" + group_name + "," + "5.krog " + group[4] + ":" + group[0],
            tracker_id=3,
            description="Skupina:" + group_name + "," + "5.krog " + group[4] + ":" + group[0],
            category_id=ids_categories[category],
            status_id=1,
            parent_issue_id=parent_issue_id,
            priority_id=1,
            assigned_to_id=1,
            watcher_user_ids=[1],
            custom_fields=custom_fields_match

        )

def generate_wiki_table(headers, data):
    """
    Generates a wiki-style table given the headers and data.

    :param headers: A list of strings representing the headers for the table.
    :param data: A list of lists representing the data for the table.
    :return: A string representing the generated table.


    headers = ["#", "Group :A", "1", "2", "3", "4", "Nizi", "Točke", "Mesto"]
    data = [
        [1, "John Smith", "/", "/", "/", "/", "/", "/", "/"],
        [2, "Jane Doe", "/", "/", "/", "/", "/", "/", "/"],
        [3, "Bob Johnson", "/", "/", "/", "/", "/", "/", "/"],
        [4, "Alice Lee", "/", "/", "/", "/", "/", "/", "/"]
    ]

    table = generate_wiki_table(headers, data)
    print(table)
    """

    # Build the table header
    table_header = "|" + "|".join(headers) + "|\n"

    # Build the table separator
    num_cols = len(headers)
    table_separator = "|" + "|".join(["---"] * num_cols) + "|\n"

    # Build the table rows
    table_rows = ""
    for row in data:
        table_rows += "|" + "|".join(map(str, row)) + "|\n"

    # Combine the header, separator, and rows to create the final table string
    table = table_header + table_separator + table_rows

    return table